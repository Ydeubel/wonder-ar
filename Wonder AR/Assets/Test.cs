﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public List<GameObject> tours;
    public List<Vector3> toursPos;
    public List<Vector3> scaleO;
    private Vector3 scaleChange;
    public GameObject back;
    public GameObject growth;
    public GameObject shrink;
    public GameObject panelT;
    public GameObject panelM;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        scaleChange = new Vector3(0.5f, 0.5f, 0.5f);
    }
    void OnMouseDown()
    {
        //panel.SetActive(true);
        foreach (GameObject go in tours)
        {
            toursPos.Add(go.transform.localPosition);
            scaleO.Add(go.transform.localScale);
            if (go != transform.gameObject)
            {
                go.SetActive(false);
            }
            else
            {
                panelM.SetActive(false);
                panelT.SetActive(true);
                transform.localPosition = Vector3.zero;
                back.SetActive(true);
                growth.SetActive(true);
                shrink.SetActive(true);
            }
        }
    }

    public void Back()
    {
        panelM.SetActive(true);
        panelT.SetActive(false);
        back.SetActive(false);
        foreach (GameObject go in tours)
        {

            go.SetActive(true);


            for (int i = 0; i < tours.Count; i++)
            {
                tours[i].transform.localPosition = toursPos[i];
                tours[i].transform.localScale = scaleO[i];

            }
        }
        back.SetActive(false);
        growth.SetActive(false);
        shrink.SetActive(false);

    }

    public void Growth()
    {

        foreach (GameObject go in tours)
        {
            go.transform.localScale += scaleChange;
        }
    }

    public void Shrink()
    {

        foreach (GameObject go in tours)
        {
            go.transform.localScale -= scaleChange;
        }
    }
}
